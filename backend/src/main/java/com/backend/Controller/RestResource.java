package com.backend.Controller;

import com.backend.entity.User;
import com.backend.repository.UserRepository;
import com.backend.repository.UserRoleRepository;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created in IntelliJ IDEA.
 * User: Daniel QuirozV
 * Date: 8/27/2017
 * Time: 1:55 PM
 */
@RestController
public class RestResource {

    private final Log LOG = LogFactory.getLog(RestResource.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private BCryptPasswordEncoder encoder;

    @PostMapping("/singUp")
    public void singUpUser(@RequestBody User user){
        String passEncode = encoder.encode(user.getPassword());
        user.setPassword(passEncode);

        userRoleRepository.save(user.getUserRoles());
        LOG.info("Usuario Guardado");
        userRepository.save(user);
    }

}
