package com.backend.Services;

import com.backend.entity.User;
import com.backend.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;



/**
 * Created in IntelliJ IDEA.
 * User: Daniel QuirozV
 * Date: 8/27/2017
 * Time: 4:50 PM
 */
@Service
public class UserSecurityService implements UserDetailsService{

    private static final Logger LOG = LoggerFactory.getLogger(UserSecurityService.class);

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(s);

        if (null ==user){
            LOG.warn("Nombre usuario no encontrado");
            throw  new UsernameNotFoundException("Username "+s);
        }else {

        }

        return user;
    }
}
