package com.backend.config;

import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

/**
 * Created in IntelliJ IDEA.
 * User: Daniel QuirozV
 * Date: 8/27/2017
 * Time: 5:01 PM
 */
@Component
public class AuthFailureHandler extends SimpleUrlAuthenticationFailureHandler {
}
