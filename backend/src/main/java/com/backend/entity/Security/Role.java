package com.backend.entity.Security;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.HashSet;
import java.util.Set;

/**
 * Created in IntelliJ IDEA.
 * User: Daniel QuirozV
 * Date: 8/27/2017
 * Time: 5:45 PM
 */
@Document
public class Role {

    private String id;
    private String name;
    private Set<UserRoles> userRoles = new HashSet<>();

    public Role(){

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<UserRoles> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(Set<UserRoles> userRoles) {
        this.userRoles = userRoles;
    }
}
