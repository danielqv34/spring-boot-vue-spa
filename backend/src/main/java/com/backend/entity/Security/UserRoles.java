package com.backend.entity.Security;

import com.backend.entity.User;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created in IntelliJ IDEA.
 * User: Daniel QuirozV
 * Date: 8/27/2017
 * Time: 5:42 PM
 */
@Document
public class UserRoles {

    @Id
    private String id;

    @DBRef
    private User user;

    @DBRef
    private Role role;

    public UserRoles(User user , Role role){
        this.user = user;
        this.role = role;
    }

    public UserRoles(){

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
