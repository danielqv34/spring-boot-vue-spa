package com.backend.repository;

import com.backend.entity.Message;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by Daniel Quiroz on 6/5/2017.
 */
@RepositoryRestResource(collectionResourceRel = "messages", path = "messages")
public interface MessageRepository extends MongoRepository<Message, String> {
}
