package com.backend.repository;

import com.backend.entity.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created in IntelliJ IDEA.
 * User: Daniel QuirozV
 * Date: 8/27/2017
 * Time: 2:01 PM
 */
@RepositoryRestResource(collectionResourceRel = "user", path = "user")
public interface UserRepository extends MongoRepository<User, String> {

    User findByUsername(@Param("username") String username);

}
