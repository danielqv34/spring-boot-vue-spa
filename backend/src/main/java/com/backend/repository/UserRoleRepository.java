package com.backend.repository;

import com.backend.entity.Security.UserRoles;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created in IntelliJ IDEA.
 * User: Daniel QuirozV
 * Date: 8/27/2017
 * Time: 6:28 PM
 */
@RepositoryRestResource(collectionResourceRel = "userRoles", path = "userRoles")
public interface UserRoleRepository extends MongoRepository<UserRoles, String> {
}
