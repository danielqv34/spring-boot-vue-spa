import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

// ===================== Pages Components ======================
import Navbar from './components/Navbar.vue'
import MainPanel from './components/Main-panel.vue'
import SingUp from './components/SingUp'
import Login from './components/Login'

// ==================== Router registration ====================
export default new Router({
  mode: 'hash',
  routes: [
    { path: '/', component: MainPanel },
    { path: '/signup', component: SingUp },
    { path: '/login', component: Login }
  ]
})
